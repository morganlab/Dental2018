This repository contains the supporting code for:

Benic GZ, Farella M, Morgan XC, Viswam J, Heng NC, Cannon RD, Mei L.

Oral probiotics reduce halitosis in patients wearing orthodontic braces: a randomized, triple-blind, placebo-controlled trial.

J Breath Res. 2019 May 31;13(3):036010. 

doi: 10.1088/1752-7163/ab1c81.

https://www.ncbi.nlm.nih.gov/pubmed/31022704

Abstract:

Orthodontic braces can impede oral hygiene and promote halitosis. The aim of the study was to investigate the effect of the oral probiotic Streptococcus salivarius M18 on oral hygiene indices and halitosis in patients wearing orthodontic braces. The study was a prospective, randomized, triple-blind, placebo-controlled trial. Patients undergoing fixed orthodontic treatment were randomly allocated to a probiotic group (n = 32) and a placebo group (n = 32). Patients consumed 2 lozenges d-1 for one month. Assessments were taken at baseline, at the end of the intervention, and at a 3 month follow-up. The outcome measures were plaque index (PI), gingival index (GI) and halitosis-causing volatile sulfur compound (VSC) levels. The dental biofilms before and after the intervention were analyzed utilizing next-generation sequencing of bacterial 16S rRNA genes. PI and GI scores were not significantly influenced by the probiotic intervention (intervention × time: p > 0.05). The level of VSCs decreased significantly in both the probiotic group (VSC reduction = -8.5%, 95%CI = -7.4% to -9.1%, p = 0.015) and the placebo group (-6.5%, 95%CI = -6.0% to -7.4%, p = 0.039) after 1 month intervention. However, at the 3 month follow-up, the VSC levels in the placebo group returned to baseline levels whereas those in the probiotic group decreased further (-10.8%, 95%CI = -10.5% to -12.9%, p = 0.005). Time, but not treatment, was associated with the decrease in microbial community alpha diversity and a modest effect on beta diversity. Oral probiotic S. salivarius M18 reduced the level of halitosis in patients with orthodontic braces, but had minimal effects on PI, GI and dental biofilm microflora.


